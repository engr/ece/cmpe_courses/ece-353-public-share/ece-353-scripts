#!/bin/sh

BLUE='\033[0;34m'
WHITE='\033[0;37m'
RED='\033[0;31m'
CYAN='\033[0;36m'
ORANGE='\033[0;33m'
GREEN='\033[0;32m'

echo -e "************************************************************************"
echo -e "          ${CYAN}ECE 353 - Introduction to Microprocessor Systems${WHITE}"
echo -e "************************************************************************"
echo -e "This script will set your Git credentials so that you do not have to"
echo -e "specify your username and GitLabs token each time that you access "
echo -e "gitlabs "
read -p "Do you want to proceed? (y/n)" yn

read -p 'Please enter GitLab username: ' USERNAME
read -p 'Please enter GitLab Personal Access Token: ' PAT
git config --global credential."https://git.doit.wisc.edu".helper store --replace-all 
GITHUB_USER=$USERNAME 
GITHUB_TOKEN=$PAT
echo "https://$GITHUB_USER:$GITHUB_TOKEN@git.doit.wisc.edu" >> ~/.git-credentials

echo -e "${GREEN}[STATUS] GitLab token configured successfully${WHITE}"
echo -e
echo -e "${CYAN}[STATUS] Setup script complete${WHITE}"
